﻿using System.Data.Entity;
using System.Data.Entity.SqlServer;

namespace DynamicForms.DAL
{
	public class FormConfiguration : DbConfiguration
	{
		public FormConfiguration()
		{
			SetExecutionStrategy("System.Data.SqlClient", () => new SqlAzureExecutionStrategy());
		}
	}
}