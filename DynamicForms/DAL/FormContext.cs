﻿using DynamicForms.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using ValueType = DynamicForms.Models.ValueType;

namespace DynamicForms.DAL
{
	public class FormContext : DbContext
	{
		public DbSet<Form> Forms { get; set; }
		public DbSet<Field> Fields { get; set; }
		public DbSet<Models.Type> Types { get; set; }
		public DbSet<ValueType> ValueTypes { get; set; }
		
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

			modelBuilder.Entity<Form>()
				.HasMany(c => c.Fields).WithMany(i => i.Forms)
				.Map(t => {
					t.MapLeftKey("FormID")
					.MapRightKey("FieldID")
					.ToTable("FormField");
				});

			modelBuilder.Entity<ValueType>()
				.HasRequired(s => s.Type)
				.WithMany()
				.WillCascadeOnDelete(false);
		}
	}
}