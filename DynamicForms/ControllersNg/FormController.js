﻿angular.module('FormApp', [])
    .directive('startdate', function () {
        return {
            require: 'ngModel',
            link: function (scope, elem, attr, ngModel) {

                ngModel.$parsers.unshift(function (value) {
                    var startDate = scope.convertStringToDate(value);
                    var dateNow = new Date();

                    var valid = startDate >= dateNow;
                    ngModel.$setValidity('startdate', valid);
                    return valid ? value : undefined;
                });

                ngModel.$formatters.unshift(function (value) {
                    var startDate = scope.convertStringToDate(value);
                    var dateNow = new Date();

                    var valid = startDate >= dateNow;
                    ngModel.$setValidity('startdate', valid);
                    return value;
                });
            }
        };
    })
    .directive('enddate', function () {
        return {
            require: 'ngModel',
            link: function (scope, elem, attr, ngModel) {

                ngModel.$parsers.unshift(function (value) {
                    var startDate = scope.frmForm.startingValidity.$modelValue;
                    var endDate = scope.convertStringToDate(value);

                    var valid = endDate > startDate;
                    ngModel.$setValidity('enddate', valid);
                    return valid ? value : undefined;
                });

                ngModel.$formatters.unshift(function (value) {
                    var startDate = scope.frmForm.startingValidity.$modelValue;
                    var endDate = scope.convertStringToDate(value);

                    var valid = endDate > startDate;
                    ngModel.$setValidity('enddate', valid);
                    return value;
                });
            }
        };
    })
    .controller('FormController', ['$scope', '$http', '$location', function ($scope, $http, $location) {
        $scope.form = {};
        $scope.types = [];
        $scope.valueTypes = [];
        $scope.form.Fields = [];
        $scope.message = '';

        $scope.addNewField = function () {
            $scope.form.Fields.push({});
        };

        $scope.removeField = function (index) {
            $scope.form.Fields.splice(index, 1);
            $scope.valueTypes.splice(index, 1);
        };

        $scope.getTypes = function () {
            $http.get('/Types/GetTypes').then(
                function (successResponse) {
                    $scope.types = JSON.parse(successResponse.data);
                },
                function (errorResponse) {
                    console.log(errorResponse);
                });
        };

        $scope.getValueTypesByType = function (pTypeId, index) {

            var params = {
                typeId: pTypeId
            };

            $http.get('/ValueTypes/GetValueTypesByTypeId', { params: params }).then(
                function (successResponse) {
                    $scope.valueTypes[index] = JSON.parse(successResponse.data);
                },
                function (errorResponse) {
                    console.log(errorResponse);
                });
        };

        $scope.setTypeId = function (typeID, index) {
            $scope.getTypes();

            if (typeID) {
                $scope.getValueTypesByType(typeID, index);
            }

            return typeID;
        }

        $scope.submitForm = function() {
            $scope.formLoading = true;
            var urlPost = $scope.isEdit ? '/Forms/Edit' : '/Forms/Create';

            $http({
                method: 'POST',
                url: urlPost,
                data: $scope.form
            }).then(function (result) {
                $scope.errors = [];
                if (result.data.success === true) {
                    if (!$scope.isEdit) {
                        $scope.form = {};
                        $scope.form.Fields = [];

                        $scope.frmForm.startingValidity.$setPristine();
                        $scope.frmForm.endingValidity.$setPristine();
                    }
                    $scope.message = 'Operação realizada com sucesso!';
                }
                else {
                    $scope.errors = data.errors;
                }
            }, function (result) {
                $scope.errors = [];
                $scope.message = 'Erro';
            });
            $scope.formLoading = false;
        }

        $scope.customValidity = function (frmForm) {
            if ($scope.form.Fields.length < 10) {
                return true;
            } else {
                return frmForm.$invalid;
            }
        }

        $scope.convertStringToDate = function (date) {
            var myDate = date;
            myDate = myDate.split("-");
            var newDate = myDate[1] + "/" + myDate[2] + "/" + myDate[0];

            return new Date(newDate);
        }

        $scope.FormName = "Criar";
        var pId = $location.absUrl().split("/").pop();
        if (!isNaN(pId)) {
            var params = {
                id: pId
            };

            $http.get('/Forms/GetItem', { params: params }).then(
                function (successResponse) {
                    $scope.form = JSON.parse(successResponse.data);

                    $scope.form.StartingValidity = new Date($scope.form.StartingValidity);
                    $scope.form.EndingValidity = new Date($scope.form.EndingValidity);

                    $scope.isEdit = true;
                    $scope.FormName = "Editar";
                },
                function (errorResponse) {
                    console.log(errorResponse);
                });
        }
    }])
