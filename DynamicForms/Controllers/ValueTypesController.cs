﻿using Newtonsoft.Json;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using FormContext = DynamicForms.DAL.FormContext;

namespace DynamicForms.Controllers
{
	public class ValueTypesController : Controller
    {
        private FormContext db = new FormContext();

        // GET: ValueTypes
        public ActionResult Index()
        {
            return View(db.ValueTypes.ToList());
        }

		public ActionResult GetValueTypesByTypeId(int typeId)
		{
			var valueTypes = 
				JsonConvert.SerializeObject(db.ValueTypes.Where(v => v.TypeID == typeId).ToList());

			return Json(valueTypes, JsonRequestBehavior.AllowGet);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
