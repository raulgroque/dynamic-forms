﻿using Newtonsoft.Json;
using System.Linq;
using System.Web.Mvc;
using FormContext = DynamicForms.DAL.FormContext;

namespace DynamicForms.Controllers
{
	public class TypesController : Controller
    {
        private FormContext db = new FormContext();

        // GET: Types
        public ActionResult Index()
        {
            return View(db.Types.ToList());
        }

		public ActionResult GetTypes()
		{
			var types = JsonConvert.SerializeObject(db.Types.ToList(), 
				new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

			return Json(types, JsonRequestBehavior.AllowGet);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
