﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DynamicForms.DAL;
using DynamicForms.Models;
using FormContext = DynamicForms.DAL.FormContext;
using PagedList;
using Newtonsoft.Json;
using System.Data.Entity.Migrations;

namespace DynamicForms.Controllers
{
    public class FormsController : Controller
    {
		private FormContext db = new FormContext();

        // GET: Forms/Index
        public ActionResult Index()
        {
			return View(db.Forms.ToList());
        }

		// GET: Forms/List
		public ActionResult List(int? selectedStatus, string sortOrder, int? page)
		{
			ViewBag.CurrentSort = sortOrder;
			ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
			ViewBag.StartingValiditySortParm = String.IsNullOrEmpty(sortOrder) ? "start_valid_desc" : "";
			ViewBag.EndingValiditySortParm = String.IsNullOrEmpty(sortOrder) ? "end_valid_desc" : "";
			ViewBag.SelectedStatusID = selectedStatus.GetValueOrDefault();

			ViewBag.SelectedStatus = new SelectList(
				new List<SelectListItem>
				{
					new SelectListItem { Selected = false, Text = "Ativo", Value = "1"},
					new SelectListItem { Selected = false, Text = "Inativo", Value = "2"},
				}, "Value", "Text", selectedStatus);

			List<Form> forms = new List<Form>();
			if (!selectedStatus.HasValue || selectedStatus == 0)
				forms = db.Forms.ToList();
			else if (selectedStatus == 1)
				forms = db.Forms.
					Where(c => c.StartingValidity <= DateTime.Now && c.EndingValidity >= DateTime.Now).ToList();
			else
				forms = db.Forms.
					Where(c => c.StartingValidity > DateTime.Now || c.EndingValidity < DateTime.Now).ToList();

			switch (sortOrder)
			{
				case "name_desc":
					forms = forms.OrderByDescending(s => s.Name).ToList();
					break;
				case "start_valid_desc":
					forms = forms.OrderBy(s => s.StartingValidity).ToList();
					break;
				case "end_valid_desc":
					forms = forms.OrderByDescending(s => s.EndingValidity).ToList();
					break;
				default:
					forms = forms.OrderBy(s => s.Name).ToList();
					break;
			}

			int pageSize = 10;
			int pageNumber = (page ?? 1);
			return View(forms.ToPagedList(pageNumber, pageSize));
		}

		public JsonResult GetItem(int? id)
		{
			if (id == null)
			{
				return Json(new { success = false });
			}
			Form form = db.Forms.Find(id);
			if (form == null)
			{
				return Json(new { success = false });
			}

			return Json(JsonConvert.SerializeObject(form,
				new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }),
				JsonRequestBehavior.AllowGet);
		}

		public ActionResult Create()
		{
			return View();
		}

		[HttpPost]
		public JsonResult Create(Form form)
		{
			if (ModelState.IsValid)
			{
				db.Forms.Add(form);
				db.SaveChanges();

				var RedirectUrl = Url.Action("Index", "Home");
				return Json(new { success = true, redirectUrl = RedirectUrl });
			}
			return Json(new
			{
				success = false
			});
		}

		[HttpPost]
		public JsonResult Edit(Form form)
		{
			if (ModelState.IsValid)
			{
				var formDb = db.Forms.Find(form.FormID);

				if (TryUpdateModel(formDb, "",
					new string[] { "Name", "StartingValidity", "EndingValidity", "Fields" }))
				{
					try
					{
						db.SaveChanges();

						var RedirectUrl = Url.Action("Index", "Home");
						return Json(new { success = true, redirectUrl = RedirectUrl });
					}
					catch
					{
						return Json(new { success = false });
					}
				}
			}
			return Json(new { success = false });
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
