namespace DynamicForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class formcontext : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Field",
                c => new
                    {
                        FieldID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        TypeID = c.Int(nullable: false),
                        ValueTypeID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.FieldID);
            
            CreateTable(
                "dbo.Form",
                c => new
                    {
                        FormID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        StartingValidity = c.DateTime(),
                        EndingValidity = c.DateTime(),
                    })
                .PrimaryKey(t => t.FormID);
            
            CreateTable(
                "dbo.Type",
                c => new
                    {
                        TypeID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.TypeID);
            
            CreateTable(
                "dbo.ValueType",
                c => new
                    {
                        ValueTypeID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        TypeID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ValueTypeID)
                .ForeignKey("dbo.Type", t => t.TypeID)
                .Index(t => t.TypeID);
            
            CreateTable(
                "dbo.FormField",
                c => new
                    {
                        FormID = c.Int(nullable: false),
                        FieldID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.FormID, t.FieldID })
                .ForeignKey("dbo.Form", t => t.FormID, cascadeDelete: true)
                .ForeignKey("dbo.Field", t => t.FieldID, cascadeDelete: true)
                .Index(t => t.FormID)
                .Index(t => t.FieldID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ValueType", "TypeID", "dbo.Type");
            DropForeignKey("dbo.FormField", "FieldID", "dbo.Field");
            DropForeignKey("dbo.FormField", "FormID", "dbo.Form");
            DropIndex("dbo.FormField", new[] { "FieldID" });
            DropIndex("dbo.FormField", new[] { "FormID" });
            DropIndex("dbo.ValueType", new[] { "TypeID" });
            DropTable("dbo.FormField");
            DropTable("dbo.ValueType");
            DropTable("dbo.Type");
            DropTable("dbo.Form");
            DropTable("dbo.Field");
        }
    }
}
