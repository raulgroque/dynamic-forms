﻿namespace DynamicForms.Migrations
{
	using DynamicForms.DAL;
	using DynamicForms.Models;
	using System;
	using System.Collections.Generic;
	using System.Data.Entity.Migrations;
	using Type = Models.Type;
	using ValueType = Models.ValueType;

	internal sealed class Configuration : DbMigrationsConfiguration<FormContext>
	{
		public Configuration()
		{
			AutomaticMigrationsEnabled = true;
			AutomaticMigrationDataLossAllowed = true;
		}

		protected override void Seed(FormContext context)
		{
			var types = new List<Type>
			{
				new Type { Description = "Textbox" },
				new Type { Description = "Dropbox" },
				new Type { Description = "Radio Button" }
			};
			types.ForEach(s => context.Types.AddOrUpdate(p => p.Description, s));
			context.SaveChanges();

			types.ForEach(s => context.Types.AddOrUpdate(p => p.Description, s));
			var valueTypes = new List<ValueType>
			{
				new ValueType { Description = "Texto", TypeID = 1 },
				new ValueType { Description = "Numérico", TypeID = 1 },
				new ValueType { Description = "Moeda", TypeID = 1 },
				new ValueType { Description = "Data", TypeID = 1 },
				new ValueType { Description = "Padrão", TypeID = 2 },
				new ValueType { Description = "Padrão", TypeID = 3 }
			};
			valueTypes.ForEach(s => context.ValueTypes.AddOrUpdate(p => p.Description, s));
			context.SaveChanges();

			var fields = new List<Field>
			{
				new Field { Description = "Teste 1", TypeID = 1, ValueTypeID = 1 },
				new Field { Description = "Teste 2", TypeID = 1, ValueTypeID = 2 },
				new Field { Description = "Teste 3", TypeID = 1, ValueTypeID = 3 },
				new Field { Description = "Teste 4", TypeID = 1, ValueTypeID = 4 },
				new Field { Description = "Teste 5", TypeID = 2, ValueTypeID = 5 },
				new Field { Description = "Teste 6", TypeID = 3, ValueTypeID = 6 },
				new Field { Description = "Teste 7", TypeID = 1, ValueTypeID = 1 },
				new Field { Description = "Teste 8", TypeID = 1, ValueTypeID = 2 },
				new Field { Description = "Teste 9", TypeID = 1, ValueTypeID = 3 },
				new Field { Description = "Teste 10", TypeID = 1, ValueTypeID = 4 },
			};
			var forms = new List<Form>
			{
				new Form {
					Name = "Teste",
					StartingValidity = DateTime.Now.AddDays(1),
					EndingValidity = DateTime.Now.AddDays(5),
					Fields = fields
				}
			};

			forms.ForEach(s => context.Forms.AddOrUpdate(p => p.Name, s));
			context.SaveChanges();
		}
	}
}
