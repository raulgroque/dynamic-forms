﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DynamicForms.Models
{
	public class Form
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int FormID { get; set; }

		public string Name { get; set; }

		public DateTime? StartingValidity { get; set; }

		public DateTime? EndingValidity { get; set; }

		public virtual ICollection<Field> Fields { get; set; }
	}
}