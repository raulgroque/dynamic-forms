﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DynamicForms.Models
{
	public class ValueType
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ValueTypeID { get; set; }

		public string Description { get; set; }

		public int TypeID { get; set; }

		public virtual Type Type { get; set; }
	}
}