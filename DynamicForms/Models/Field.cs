﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DynamicForms.Models
{
	public class Field
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int FieldID { get; set; }

		public string Description { get; set; }

		public int TypeID { get; set; }

		public int ValueTypeID { get; set; }

		public virtual ICollection<Form> Forms { get; set; }
	}
}